#ifndef __BASE_CALCULATE_CLASSES_HPP_
#define __BASE_CALCULATE_CLASSES_HPP_
#include <boost/uuid/uuid.hpp>            
#include <boost/uuid/uuid_generators.hpp> 
#include <boost/uuid/uuid_io.hpp> 
#include <functional>
#include <vector>

#include <iostream>
///////////////////////////////////////////////////////////////////////
//Some defines
using RecordId = boost::uuids::uuid;

///////////////////////////////////////////////////////////////////////
//Interface for iterative solver

struct IterativeSolverOutput
    {
    unsigned int number_of_steps;
    std::vector<double*> data;
    /*first pointer is massive of time values over are pointers for
     * quantities set by AddOutputQuantity function */
    };
template<typename DATA>
using SolverInit = std::function<void(DATA * /*coordinate massive to init*/)>;

template<typename DATA>
using TimeDerivative = std::function<void(const DATA */*input coords*/,DATA * /*derivative result*/,const double/*current time*/)>;

template<typename DATA>
using SolverOutputQuantities = std::function<void(const DATA */*current coords*/,double * /*Output massive*/)>;

template<typename DATA>
class IIterativeComplexSolver
    {
    public:
        virtual ~IIterativeComplexSolver(){};
        virtual void SetDimension(const unsigned int)=0;
        virtual void SetTimeStep(const double)=0;
        virtual void SetInitFunctor(SolverInit<DATA>)=0;
        virtual void SetRightPart(TimeDerivative<DATA>)=0;
        virtual void SetOutputChanelsNumber(const unsigned int)=0;
        virtual void SetOutputMaker(SolverOutputQuantities<DATA>)=0;
        virtual void calculate(void)=0;
        virtual void add_steps(const unsigned int&)=0;
        virtual IterativeSolverOutput GetOutput(void)=0;
        virtual DATA* GetCurrentCoords()=0;
    };

///////////////////////////////////////////////////////////////////////
//Caching

class ICache
    {
    public:
        virtual ~ICache(){};
        virtual void SetIsCached(bool)=0;
    };

#endif /* __BASE_CALCULATE_CLASSES_HPP_ */
