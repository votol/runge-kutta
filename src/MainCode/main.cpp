#include <iostream>
#include <math.h>
#include <string>
#include <thread>
#include <stdexcept>
#include <chrono>
#include "MathVector.hpp"
#include "complex.hpp"
#include "RungeKutt.hpp"
#define ThreadNum 1


class AsymParametric
    {
        complex<double> _i=complex<double>(0.0,1.0);
    public:
        int dimension=6;
        double dt=0.001;
        double eps=0.35;
        double g=0.1;
        double G=0.1;
        double nt=0.0001;
        double D=0.0; 
        double dT=0.0;
        void init(complex<double> *in)
            {
            in[0]=complex<double>(0.5,0.0);
            in[1]=complex<double>(0.5,0.0);
            in[2]=complex<double>(0.5,0.0);
            in[3]=complex<double>(0.0,0.0);
            in[4]=complex<double>(0.0,0.0);
            in[5]=complex<double>(0.0,0.0);
            }
        void derivative(const complex<double> *in,complex<double> *out,const double t)
            {
            out[0]=-G*in[0]-eps*in[3].__re+2*g*in[2].__im+G*(nt+dT);
            out[1]=-G*in[1]-2*g*in[2].__im+G*nt;
            out[2]=-G*in[2]-0.5*eps*in[5]-_i*g*(in[0]-in[1]);
            out[3]=-(G+D)*in[3]-0.5*eps*(2.0*in[0]+complex<double>(1.0,0.0))-2.0*_i*g*in[5];
            out[4]=-(G+D)*in[4]-2.0*_i*g*in[5];
            out[5]=-(G+D)*in[5]-0.5*eps*in[2]-_i*g*(in[3]+in[4]);
            
            /*(*out)[6]=pp[0]*(-0.5*pp[3]*(*in)[6]-0.5*pp[1]*(*in)[8]-_i*pp[2]*(*in)[7]);
            (*out)[7]=pp[0]*(-0.5*pp[3]*(*in)[7]-_i*pp[2]*(*in)[6]);
            (*out)[8]=pp[0]*(-(0.5*pp[3]+pp[5])*(*in)[8]-0.5*pp[1]*(*in)[6]+_i*pp[2]*(*in)[9]);
            (*out)[9]=pp[0]*(-(0.5*pp[3]+pp[5])*(*in)[9]+_i*pp[2]*(*in)[8]);
            */
            }
        void output1(const complex<double> *in, double *out)
            {
            out[0]=2*sqrt(abs2<double>(in[2]))/(in[0].__re+in[1].__re);
            }
    };

double answer[128];
std::mutex Mutex;


void thread_fun(unsigned int i)
    {
    for(int k=0;k<4;k++)    
        {
        AsymParametric task;
        RungeKuttSolver<complex<double>,MathVector<complex<double> > > solv;
        IterativeSolverOutput per;
        task.eps=0.6*double(k*ThreadNum+i)/128.0;
        solv.SetDimension(task.dimension);
        solv.SetTimeStep(task.dt);
        solv.SetInitFunctor(std::bind(&AsymParametric::init,&task,std::placeholders::_1));
        solv.SetRightPart(std::bind(&AsymParametric::derivative,&task,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
        solv.SetOutputChanelsNumber(1);
        solv.SetOutputMaker(std::bind(&AsymParametric::output1,&task,std::placeholders::_1,std::placeholders::_2));
        solv.calculate();
        solv.add_steps(200000);
        per=solv.GetOutput();
        Mutex.lock();
        answer[k*ThreadNum+i]=per.data[1][199999];
        Mutex.unlock();
        delete [] per.data[0];
        delete [] per.data[1];
        }
    }


int main(int argc,char** argv)
	{
	auto t1 = std::chrono::high_resolution_clock::now();
    std::thread mas[ThreadNum];
    for(int i=0;i<ThreadNum;i++)
        mas[i]=std::thread(thread_fun,i);
    for(int i=0;i<ThreadNum;i++)
        mas[i].join();
    //RungeKuttComplexSolver::CacheToCout();
    //std::cout<<RungeKuttComplexSolver::count_calculate<<std::endl;
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "Programm worked:" <<
                std::chrono::duration<double>(t2 - t1).count() 
                <<" seconds"<<std::endl;
    /*for(int i=0;i<128;i++)
        {
        std::cout<<answer[i]<<std::endl;
        }*/
    return 1;
	}
