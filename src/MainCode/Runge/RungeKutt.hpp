#ifndef __RungeKutt_hpp_
#define __RungeKutt_hpp_
#include <map>
#include <list>
#include <mutex>
#include "base_classes.hpp"



template<typename DATA, typename Vector>
class RungeKuttSolver:public IIterativeComplexSolver<DATA>
    {
    private:    
        std::mutex SetMutex;
        std::mutex CalculatorMutex;
        ///////////////////////////////////////////////////////////////
        ///For calculation
        unsigned int CurrentStep;
        RecordId CurrentRec;
        Vector teck;
        Vector per_vector[4];
        double Time;
        
        
        ///////////////////////////////////////////////////////////////
        ///Calculation parametrs
        double TimeStep;
        unsigned int DimensionTmp;
        unsigned int Dimension;
        unsigned int NumberOfChanelsTmp;
        unsigned int NumberOfChanels;
        SolverInit<DATA> Init;
        TimeDerivative<DATA> DerivativeTmp;
        TimeDerivative<DATA> Derivative;
        SolverOutputQuantities<DATA> OutputChanels;
        SolverOutputQuantities<DATA> OutputChanelsTmp;
        
        
        ///////////////////////////////////////////////////////////////
        ///For output
        std::list<IterativeSolverOutput> output;
        unsigned int number_of_blocks;
        ///////////////////////////////////////////////////////////////
    public:
        static int count_calculate;
        RungeKuttSolver()
            {
            NumberOfChanels=0;
            Dimension=0;
            NumberOfChanelsTmp=0;
            DimensionTmp=0;
            CurrentStep=0;
            TimeStep=0;
            number_of_blocks=0;
            CurrentRec=boost::uuids::nil_generator()();
            }
        
        virtual ~RungeKuttSolver()
            {
            CalculatorMutex.lock();
            for(auto it=output.begin();it!=output.end();++it)
                {
                for(auto it1=it->data.begin();it1!=it->data.end();++it1)
                    {
                    delete [] *it1;
                    }
                }
            CalculatorMutex.unlock();
            }
        
        ///////////////////////////////////////////////////////////////
        ///IIterativeComplexSolver
        virtual void SetDimension(const unsigned int N)
            {
            SetMutex.lock();
            DimensionTmp=N;
            SetMutex.unlock();
            }
        
        virtual void SetTimeStep(const double dt)
            {
            SetMutex.lock();
            TimeStep=dt;
            SetMutex.unlock();
            }
        
        virtual void SetInitFunctor(SolverInit<DATA> init)
            {
            SetMutex.lock();
            Init=init;
            SetMutex.unlock();
            }
        
        virtual void SetRightPart(TimeDerivative<DATA> derivative)
            {
            SetMutex.lock();
            DerivativeTmp=derivative;
            SetMutex.unlock();
            }
        
        virtual void SetOutputChanelsNumber(const unsigned int N)
            {
            SetMutex.lock();
            NumberOfChanelsTmp=N;
            SetMutex.unlock();
            }
        
        virtual void SetOutputMaker(SolverOutputQuantities<DATA> output)
            {
            SetMutex.lock();
            OutputChanelsTmp=output;
            SetMutex.unlock();
            }
        
        virtual void calculate(void)
            {
            try
                {
                SetMutex.lock();
                CalculatorMutex.lock();
                ///Clear output
                for(auto it=output.begin();it!=output.end();++it)
                    {
                    for(auto it1=it->data.begin();it1!=it->data.begin();++it1)
                        {
                        delete [] *it1;
                        }
                    }
                ///Tmp to active
                Dimension=DimensionTmp;
                NumberOfChanels=NumberOfChanelsTmp;
                OutputChanels=OutputChanelsTmp;
                Derivative=DerivativeTmp;
                
                if(!Init)
                    {
                    throw std::runtime_error("Init function doesn't exist");
                    }
                if(!Derivative)
                    {
                    throw std::runtime_error("Derivative function doesn't exist");
                    }
                if(!OutputChanels)
                    {
                    throw std::runtime_error("Output function doesn't exist");
                    }
                ///Init
                teck.resize(Dimension);
                per_vector[0].resize(Dimension);
                per_vector[1].resize(Dimension);
                per_vector[2].resize(Dimension);
                per_vector[3].resize(Dimension);
                Time=0.0;
                Init(teck.get_data());
                ///output to zeros
                CurrentStep=0;
                number_of_blocks=0;
                SetMutex.unlock();
                CalculatorMutex.unlock();
                }
            catch(std::exception& exept)
                {
                SetMutex.unlock();
                CalculatorMutex.unlock();
                throw std::runtime_error(std::string("Can't init RungeKutt calculation: ")+std::string(exept.what()));
                }
            }
        
        virtual void add_steps(const unsigned int& StepsCount)
            {
            if(StepsCount==0)
                return;
            double *per_output=0;
            unsigned int count1,count2;
                
            try
                {
                CalculatorMutex.lock();
                //std::list<IterativeComplexSolverOutputQuantity>::iterator it_output;
                ///increase output massives compasity
                number_of_blocks++;
                output.push_back(IterativeSolverOutput());
                output.back().data.resize(NumberOfChanels+1);
                per_output=new double[NumberOfChanels];
                for(count1=0;count1<NumberOfChanels+1;++count1)
                    {
                    output.back().data[count1]=new double[StepsCount];
                    }
                for(count1=0;count1<StepsCount;++count1)
                    {
                    output.back().data[0][CurrentStep]=Time;
                    OutputChanels(teck.get_data(),per_output);
                    for(count2=0;count2<NumberOfChanels;++count2)
                        {
                        output.back().data[count2+1][CurrentStep]=per_output[count2];
                        }
                    Derivative(teck.get_data(),per_vector[0].get_data(),Time);
                    per_vector[1]=per_vector[0];
                    per_vector[1]/=DATA(6.0);
                    per_vector[3]=per_vector[0];
                    per_vector[3]*=DATA(0.5*TimeStep);
                    per_vector[2]=teck;
                    per_vector[2]+=per_vector[3];
                    Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+0.5*TimeStep);
                    per_vector[3]=per_vector[0];
                    per_vector[3]/=DATA(3.0);
                    per_vector[1]+=per_vector[3];
                    per_vector[3]=per_vector[0];
                    per_vector[3]*=DATA(0.5*TimeStep);
                    per_vector[2]=teck;
                    per_vector[2]+=per_vector[3];
                    Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+0.5*TimeStep);
                    per_vector[3]=per_vector[0];
                    per_vector[3]/=DATA(3.0);
                    per_vector[1]+=per_vector[3];
                    per_vector[3]=per_vector[0];
                    per_vector[3]*=DATA(TimeStep);
                    per_vector[2]=teck;
                    per_vector[2]+=per_vector[3];
                    Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+TimeStep);
                    per_vector[3]=per_vector[0];
                    per_vector[3]/=DATA(6.0);
                    per_vector[1]+=per_vector[3];
                    per_vector[1]*=DATA(TimeStep);
                    teck+=per_vector[1];
                    Time+=TimeStep;
                    CurrentStep++;
                    }
                output.back().number_of_steps=StepsCount;
                delete per_output;
                CalculatorMutex.unlock();
                }
            catch(std::exception& exept)
                {
                if(per_output!=0)
                    delete per_output;
                CalculatorMutex.unlock();
                throw std::runtime_error(std::string("During RungeKutt calculation some error occure: ")+std::string(exept.what()));
                }
            }
        
        virtual IterativeSolverOutput GetOutput()
            {
            IterativeSolverOutput per_output;
            unsigned int count1,count2,count3;
            try
                {
                CalculatorMutex.lock();
                per_output.number_of_steps=CurrentStep;
                per_output.data.resize(NumberOfChanels+1);
                for(count1=0;count1<NumberOfChanels+1;++count1)
                    {
                    per_output.data[count1]=new double[CurrentStep]; 
                    }
                count3=0;
                for(auto it=output.begin();it!=output.end();++it)
                    {
                    for(count1=0;count1<it->number_of_steps;++count1)
                        {
                        for(count2=0;count2<NumberOfChanels+1;++count2)
                            {
                            per_output.data[count2][count3]=it->data[count2][count1];
                            }
                        count3++;
                        }
                    }
                CalculatorMutex.unlock();
                }
            catch(std::exception& exept)
                {
                CalculatorMutex.unlock();
                throw std::runtime_error(std::string("During forming RungeKutt output some error occure: ")+std::string(exept.what()));
                }
            return per_output;
            }
        
        virtual DATA* GetCurrentCoords()
            {
            return teck.get_data();
            }
    };


template<typename Data,typename Vector>
int RungeKuttSolver<Data,Vector>::count_calculate=0;
#endif /*__RungeKutt_hpp_*/
