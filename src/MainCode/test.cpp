#include <iostream>

#include <functional>

using namespace std;

using DERightPart = std::function<void(const double* /*in: cordinates*/, double* /*out: cordinates derivatives*/)>;

class IDESOlver
{
public:
    virtual ~IDESOlver() {}
    virtual setDimension(unsigned int dimension);
    virtual setRightPart(DERightPart rightPart);
    virtual double* getCordinates();
    virtual void makeStep(double dt);
    virtual void run(double timeToRun, double dt);
};

class ISolverFactory
{
public:
    virtual IDESOlver* getSolver(string solverName);
};

extern ISolverFactory* solveFactory;

using SimpleFunction = std::function<double(double)>;

class IPerElementComputer
{
public:
    virtual ~IPerElementComputer() {}
    virtual void setDimension(unsigned int dimension);
    virtual void setSource(const double*);
    virtual void setResult(double*);
    
    virtual void setOperation(SimpleFunction operation);
    virtual void syncDo();
};

std::vector<double>* uniformVectorCreator(double startPoint, double endPoint, unsigned int pointsCount)
{
    std::vector<double>* result = new std::vector<double>;
    //...
    return result;
}

class RightPartFinderOlolo
{
public:
    void getDerivatives(unsigned int pointesCount, const double* cordinates, double parameter, double* results)
    {
        
    }
}

int main()
{
    constexpr unsigned int diomension = 100;
    constexpr unsigned int pointsCount = 10;
    constexpr double startParameter = 1e-25;
    constexpr double endParameter = 6e-25;
    
    constexpr double timeToSolve = 14e87;
    constexpr double dt = 1e83;
    
    shared_ptr<std::vector<double>> parameterVector = uniformVectorCreator(startParameter, endParameter, pointsCount);
    shared_ptr<std::vector<double>> resultsVector = new vector<double>(pointsCount);
    
    IPerElementComputer *pec =  perElementComputerFactory->getPerElementComputer("signle-thread");
    
    pec->setSource(parameterVector->data());
    pec->setResult(resultsVector->data());
    
    IDESOlver* solver = new RungeKuttaSolver; //solveFactory->getSolver("runge-kutta");
    
    RightPartFinderOlolo rpf(...);
    
    // Of course, we can use function instead of lambda!
    pec->setOperation(
        [solver, &rpf](double parValue) -> double
        {
            DERightPart rightPart = std::bind(&RightPartFinderOlolo::getDerivatives, &rpf, diomension, std::placeholders::_1, parValue, std::placeholders::_2);
            // Setting here start conditions
            ...
            // Done
            
            solver->setDimension(diomension);
            solver->setRightPart(rightPart);
            solver->run(timeToSolve, dt);
            return solver->getCordinates()[2];
        }
    );
    
    pec->syncDo();
    
    return 0;
}
