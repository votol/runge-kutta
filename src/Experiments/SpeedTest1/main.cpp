#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <math.h>

class output
	{
	private:
	public:
		output(){};
		~output(){};
		virtual std::vector<double>& operator() (int ,...)=0; 
	};

double param(double t)
    {
    return exp(-(t-0.1)*(t-0.1)/(2*0.2*0.2));
    }

class functor:public output
	{
	private:
        double (*fun)(double);
        std::vector<double> output_per;
    public:
        functor()
            {
            fun=param;
            output_per.resize(2);
            }
        ~functor()
            {
            }
        std::vector<double>& operator() (int nt,...)
            {
            output_per[0]=fun(double(nt)*0.01);
            output_per[1]=double(nt)*0.01;
            return output_per;
            }
    };


int main(int argc,char** argv)
    {
    std::cout<<"OK"<<std::endl;
    return 1;
    }
